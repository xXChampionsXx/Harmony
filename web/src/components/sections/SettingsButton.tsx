import React, { InputHTMLAttributes } from 'react';
import { Flex, Text } from '@chakra-ui/react';
import { Link, useLocation } from 'react-router-dom';

type SettingsButtonProps = InputHTMLAttributes<HTMLInputElement> & {
  name: string;
  newBlock: boolean;
};

export const SettingsButton: React.FC<SettingsButtonProps> = ({ name, newBlock, ...props }) => {
  const currentPath = `/settings/${name.toLowerCase()}`;
  const location = useLocation();
  const isActive = location.pathname === currentPath;

  return (
    <Link to={currentPath}>
      <Flex
        mt={newBlock ? "2" : "0"}
        p="3"
        align="center"
        justify="space-between"
        color={isActive ? '#fff' : 'brandGray.accent'}
        _hover={{
          bg: 'brandGray.light',
          borderRadius: '5px',
          cursor: 'pointer',
          color: '#fff',
        }}
        bg={isActive ? 'brandGray.active' : undefined}
      >
        <Flex align="center">
          <Text fontSize="14px" ml="4" fontWeight="semibold">
            {name}
          </Text>
        </Flex>
      </Flex>
    </Link>
  );
};
