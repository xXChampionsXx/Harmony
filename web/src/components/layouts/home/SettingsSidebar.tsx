/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { GridItem, Box, Text } from '@chakra-ui/react';
import { SettingsButton } from '../../sections/SettingsButton';
import { dmScrollerCss } from './css/dmScrollerCSS';

export const SettingsSidebar: React.FC = () => (
  <GridItem
    gridColumn="2"
    gridRow="1 / 4"
    bg="brandGray.dark"
    overflowY="hidden"
    _hover={{ overflowY: 'auto' }}
    css={dmScrollerCss}
  >
    <Text ml="4" textTransform="uppercase" fontSize="12px" fontWeight="semibold" color="brandGray.accent" mt="4" mb="4">
      SETTINGS
    </Text>
    <Box>
      <SettingsButton name="Account" newBlock />
      <SettingsButton name="Appearance" newBlock={false} />
    </Box>
  </GridItem>
);
