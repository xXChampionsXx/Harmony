/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { GuildList } from '../guild/GuildList';
import { SettingsSidebar } from './SettingsSidebar';
import { AppLayout } from '../AppLayout';

export const SettingsAppearance: React.FC = () => (
    <AppLayout>
      <GuildList />
      <SettingsSidebar />
    </AppLayout>
);
