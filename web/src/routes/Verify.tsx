import { Box, Flex, Image } from '@chakra-ui/react';
import React from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { userStore } from '../lib/stores/userStore';
import { verify } from '../lib/api/handler/auth';

interface TokenProps {
    token: string;
}

export const Verify: React.FC = () => {
    const history = useHistory();
    const { token } = useParams<TokenProps>();
    const setUser = userStore((state) => state.setUser);
    const test = async () => {
        try {
          const { data } = await verify(
            token,
          );
          if (data) {
            setUser(data);
            history.push('/channels/me');
          }
        } catch (err: any) {
        }
      }
    test();
    return (
        <Flex minHeight="100vh" width="full" align="center" justifyContent="center">
          <Box px={4} width="full" maxWidth="500px" textAlign="center">
            <Flex mb="4" justify="center">
              <Image src={`${process.env.PUBLIC_URL}/logo.png`} w="80px" />
            </Flex>
          </Box>
        </Flex>
      );
    };