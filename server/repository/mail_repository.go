package repository

import (
	"fmt"

	"github.com/xXChampionsXx/Harmony/model"

	"encoding/base64"
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/gmail/v1"
	"google.golang.org/api/option"
)

// mailRepository contains the gmail username and password
// as well as the frontend origin.
type mailRepository struct {
	username string
	password string
	origin   string
}

// NewMailRepository is a factory for initializing Mail Repositories
func NewMailRepository(username string, password string, origin string) model.MailRepository {
	return &mailRepository{
		username: username,
		password: password,
		origin:   origin,
	}
}

// SendResetMail sends a password reset email with the given reset token
func (m *mailRepository) SendResetMail(email string, token string) error {
	messageStr := []byte("From: Harmony <" + m.username + ">\r\n" +
	"To: " + email + "\r\n" +
	"Subject: Reset Password\r\n" +
	"Content-Type: text/html; charset=\"UTF-8\"\r\n\r\n" +
	fmt.Sprintf("<a href=\"%s/reset-password/%s\">Reset Password</a>", m.origin, token))

	err := SendEmail(messageStr)

	return err
}

func (m *mailRepository) VerificationRequest(email string, token string) error {
	messageStr := []byte("From: Harmony <" + m.username + ">\r\n" +
	"To: " + email + "\r\n" +
	"Subject: Verify your Harmony Account\r\n" +
	"Content-Type: text/html; charset=\"UTF-8\"\r\n\r\n" +
	fmt.Sprintf("<a href=\"%s/verify/%s\">Verify</a>", m.origin, token))

	err := SendEmail(messageStr)

	return err
}

func SendEmail(message_raw []byte) error {
	ctx := context.Background()
	b, err := ioutil.ReadFile("credentials.json")
	if err != nil {
		log.Fatalf("Unable to read client secret file: %v", err)
	}

	// If modifying these scopes, delete your previously saved token.json.
	config, err := google.ConfigFromJSON(b, gmail.GmailSendScope)
	if err != nil {
		log.Fatalf("Unable to parse client secret file to config: %v", err)
	}
	client := getClient(config)

	gmailService, err := gmail.NewService(ctx, option.WithHTTPClient(client))
	if err != nil {
		log.Fatalf("Unable to retrieve Gmail client: %v", err)
	}

	// New message for our gmail service to send
	var message gmail.Message

	// Place messageStr into message.Raw in base64 encoded format
	message.Raw = base64.URLEncoding.EncodeToString(message_raw)

	// Send the message
	_, err = gmailService.Users.Messages.Send("me", &message).Do()
	if err != nil {
		log.Printf("Error: %v", err)
	} else {
		fmt.Println("Message sent!")
	}
	return err
}

// Retrieve a token, saves the token, then returns the generated client.
func getClient(config *oauth2.Config) *http.Client {
	// The file token.json stores the user's access and refresh tokens, and is
	// created automatically when the authorization flow completes for the first
	// time.
	tokFile := "token.json"
	tok, err := tokenFromFile(tokFile)
	if err != nil {
		tok = getTokenFromWeb(config)
		saveToken(tokFile, tok)
	}
	return config.Client(context.Background(), tok)
}

// Request a token from the web, then returns the retrieved token.
func getTokenFromWeb(config *oauth2.Config) *oauth2.Token {
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Go to the following link in your browser then type the "+
		"authorization code: \n%v\n", authURL)

	var authCode string
	if _, err := fmt.Scan(&authCode); err != nil {
		log.Fatalf("Unable to read authorization code: %v", err)
	}

	tok, err := config.Exchange(context.TODO(), authCode)
	if err != nil {
		log.Fatalf("Unable to retrieve token from web: %v", err)
	}
	return tok
}

// Retrieves a token from a local file.
func tokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	tok := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(tok)
	return tok, err
}

// Saves a token to a file path.
func saveToken(path string, token *oauth2.Token) {
	fmt.Printf("Saving credential file to: %s\n", path)
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("Unable to cache oauth token: %v", err)
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)
}